import {Navigation} from 'react-native-navigation';
import LoginScreen from './src/screens/Login';
import RegisterScreen from './src/screens/Register';
import HomeScreen from './src/screens/Home';
import RequerimientoScreen from './src/screens/Requerimiento';
import SideBarScreen from './src/components/SideBar';
import AsesoresScreen from './src/screens/Asesores';

Navigation.registerComponent("LoginScreen", () => LoginScreen);
Navigation.registerComponent("RegisterScreen", () => RegisterScreen);
Navigation.registerComponent("HomeScreen", () => HomeScreen);
Navigation.registerComponent("RequerimientoScreen", () => RequerimientoScreen);
Navigation.registerComponent("SideBarScreen", () => SideBarScreen);
Navigation.registerComponent("AsesoresScreen", () => AsesoresScreen);
Navigation.startSingleScreenApp({
  screen: {
    screen: "LoginScreen",
    title: "Login"
  }
});

