const sideBarDataMenu = [
    {
        title: 'User',
        icon: 'user',
        screen: 'HomeScreen',
        type: 'Entypo'
    },
    {
        title: 'Mis Asesorias',
        icon: 'tv',
        screen: 'RequerimientoScreen',
        type: 'Entypo'
    },
    {
        title: 'Asesorias Comunidad',
        icon: 'building',
        screen: 'HomeScreen',
        type: 'FontAwesome'
    },
    {
        title: 'Mis Calificaciones',
        icon: 'md-bookmarks',
        screen: 'HomeScreen',
        type: 'Ionicons'
    },
    {
        title: 'Gustos y preferencias',
        icon: 'priority-high',
        screen: 'HomeScreen',
        type: 'MaterialCommunityIcons'
    },
    {
        title: 'Mis referidos',
        icon: 'share-square',
        screen: 'HomeScreen',
        type: 'FontAwesome'
    },
    {
        title: 'Pago de bonos',
        icon: 'dollar',
        screen: 'HomeScreen',
        type: 'FontAwesome'
    },
    {
        title: 'Mis documentos',
        icon: 'printer',
        screen: 'HomeScreen',
        type: 'MaterialCommunityIcons'
    },
    {
        title: 'Mi guia',
        icon: 'card-giftcard',
        screen: 'HomeScreen',
        type: 'MaterialIcons'
    },
    {
        title: 'Preguntas frecuentes',
        icon: 'chat',
        screen: 'HomeScreen',
        type: 'Entypo'
    },
];

export default sideBarDataMenu;