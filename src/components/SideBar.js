import React, {Component} from 'react';
import {Container, Accordion, Content, ListItem, List, Text, Icon, Left, Body, Right, Button} from 'native-base';
import routes from './sidebarData';
const dataArray = [
    {
        title: "First Element",
        content: "Lorem ipsum dolor sit amet"
    },
    {
        title: "Second Element",
        content: "Lorem ipsum dolor sit amet"
    },
    {
        title: "Third Element",
        content: "Lorem ipsum dolor sit amet"
    }
];
export default class SideBar extends Component {
    render(){
        return (
            <Container>
            <Content>
                <List
                    dataArray={routes}
                    contentContainerStyle={{marginTop: 120}}
                    renderRow={data => {
                        return (
                            <ListItem
                                button
                                icon
                                onPress={() => {
                                    // this.props.navigator.resetTo({
                                    //     screen: data.screen,
                                    //     title: "Tu Mentor"
                                    // });
                                    this.props.navigator.handleDeepLink({ link: data.screen});
                                    this.props.navigator.toggleDrawer({
                                        side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
                                        animated: true, // does the toggle have transition animation or does it happen immediately (optional)
                                        to: 'close' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
                                    });
                                }}
                            >
                                <Left>
                                    <Icon name={data.icon} type={data.type} style={{width:40}}/>
                                </Left>
                                <Body>
                                    <Text>{data.title}</Text>
                                </Body>
                            </ListItem>
                        );
                    }}
                />
            </Content>
        </Container>
        );
    }
}