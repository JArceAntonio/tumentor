

const data = [
  {
    "title": "Curso de manejo de Redes sociales",
    "description" : "Curso para empresas en manejo de Redes sociales para incrementar sus alcances.",
    "image" : require("../assets/img1.jpeg")
  },
  {
    "title": "Publicidad para empresas",
    "description" : "Publicidad para empresas, incrementa tu alcance y crea comunidad.",
    "image" : require("../assets/img2.jpeg")
  },
  {
    "title": "Cardiologia, cuida tu salud",
    "description" : "Velamos por tu salud desde el centro de todo tu corazon y bienestar.",
    "image" : require("../assets/img3.jpeg")
  },
  {
    "title": "Marketing digital",
    "description" : "Te armamos tu plan de Marketing con los recursos que cuentes.",
    "image" : require("../assets/img4.jpeg")
  }
];

export default data;