import React , { Component, Dimensions } from 'react';
import {Image, View} from 'react-native';
import { Container, Content, Form, Item, Input, Label, Button, Text, Icon } from 'native-base';
import placeImage from '../assets/imagelogin.jpeg';
import RegisterScreen from './Register';
import {Navigation} from 'react-native-navigation';
export default class App extends Component {
  startDrawer(){
    Navigation.startSingleScreenApp({
      screen: {
        screen: "HomeScreen",
        title: "Tu Mentor",
      },
      drawer: {
        left: {
          screen: "SideBarScreen"
        }
      }
    });
  }

  render() {
    return (
      <Container >
        
        <Content padder >
        <View style={{
        justifyContent: 'center',
        alignItems: 'center',
       
      }}>
          <Image style={{width:200, height:150, marginTop:50}} source={placeImage} />
          <Text style={{fontSize: 25, marginBottom: 8}}> Tu Mentor</Text>
          <Text style={{fontSize: 16}}> Login</Text>
          <Form>
            <Item floatingLabel >
              <Label>Correo</Label>
              <Input />
            </Item>
            <Item floatingLabel last style={{marginBottom:8}}>
              <Label>Password</Label>
              <Input />
            </Item>
            <Button block style={{margin:4}} onPress={ () => {this.startDrawer()} }>
            <Text>Iniciar sesion</Text>
          </Button >
          <Button block success style={{margin:4}} onPress={() => {this.props.navigator.push({
            screen: "RegisterScreen",
            title: "Registrarse"
          })}}>
            <Text>Registrarse</Text>
          </Button >
          </Form>
        </View>
        
        </Content>
      </Container>
    );
  }
}