import React, {Component} from 'react';
import { Container, Content, Card, CardItem, Left, Thumbnail, Body, Text, List, Right, Button } from 'native-base';
import StarRating from 'react-native-star-rating';

export default class Asesores extends Component{


    constructor(props){
        super(props);
        this.state = { isLoading: true};
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    onNavigatorEvent(event) { // this is the onPress handler for the two buttons together
        if (event.type == 'DeepLink'){
            this.props.navigator.resetTo({
                screen: event.link,
                title: 'Tu Mentor'
            })
        }
    }

    componentDidMount(){
        return fetch('https://randomuser.me/api/?results=5').then( (response) => response.json())
            .then(responseJson => {
                this.setState({
                    isLoading: false,
                    dataSource: responseJson.results
                }, () => {})
            }).catch( err => console.error(err))
    }

    render(){

        if(this.state.isLoading){
            return (
                <Container>
                    <Content>
                        <Text>Cargando...</Text>
                    </Content>
                </Container>
            );
        }
        return(
            <Container>
                <Content style={{padding: 12}}>
                    <List
                        dataArray={this.state.dataSource}
                        renderRow={ (data) => {
                            return (
                                <Card>
                                    <CardItem>
                                        <Left>
                                            <Thumbnail source={{uri: data.picture.thumbnail}} />
                                            <Body>
                                                <Text>{data.name.first + ' ' + data.name.last}</Text>
                                                <Text note>{data.email}</Text>
                                                <StarRating
                                                    disabled={true}
                                                    maxStars={5}
                                                    rating={2.5}
                                                    emptyStar={'ios-star-outline'}
                                                    fullStar={'ios-star'}
                                                    halfStar={'ios-star-half'}
                                                    iconSet={'Ionicons'}
                                                    fullStarColor={'#FDD835'}
                                                    starSize={20}
                                                    containerStyle={{width: 110}}
                                                    selectedStar={(rating) => console.log(rating)}
                                                />
                                            </Body>
                                            <Right>
                                                <Button small>
                                                    <Text>Ver perfil</Text>
                                                </Button>
                                            </Right>
                                        </Left>

                                    </CardItem>
                                </Card>
                            );
                        }}
                    >

                    </List>
                </Content>
            </Container>
        );
    }
}