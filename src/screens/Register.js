import React , { Component } from 'react';
import { Navigation} from 'react-native-navigation';
import { Container, Content, Form, Item, Input, Picker, Label, Button, Text } from 'native-base';
export default class App extends Component {
    startDrawer(){
        Navigation.startSingleScreenApp({
          screen: {
            screen: "HomeScreen",
            title: "Tu Mentor"
          },
          drawer: {
            left: {
              screen: "SideBarScreen"
            }
          }
        });
      }

  render() {
    return (
      <Container >
        
        <Content padder >
            <Text style={{fontSize: 25, marginBottom: 8}}> Tu Mentor</Text>
            <Text style={{fontSize: 16}}> Registrarse</Text>
            <Form>

                <Item floatingLabel >
                    <Label>Nombre</Label>
                    <Input />
                </Item>
                <Item floatingLabel >
                    <Label>Correo</Label>
                    <Input />
                </Item>
                <Item picker style={{ marginTop: 10, marginLeft:14, marginRight: 14}}>

                    <Label>Profesion</Label>
                    <Picker
                        mode="dropdown"


                    >
                        <Picker.Item label="Ing en sistemas" value="key0" />
                        <Picker.Item label="Ing Industrial" value="key1" />
                        <Picker.Item label="Ing Comercial" value="key2" />
                        <Picker.Item label="Administracion" value="key3" />
                        <Picker.Item label="Contabilidad" value="key4" />
                    </Picker>
                </Item>

                <Item floatingLabel style={{marginBottom:8}}>
                    <Label>Celular</Label>
                    <Input />
                </Item>
                <Item picker style={{ marginTop: 10, marginLeft:14, marginRight: 14}}>

                    <Label>Pais</Label>
                    <Picker
                        mode="dropdown"


                    >
                        <Picker.Item label="Bolivia" value="key0" />
                        <Picker.Item label="Chile" value="key1" />
                        <Picker.Item label="Brasil" value="key2" />
                        <Picker.Item label="Argentina" value="key3" />
                        <Picker.Item label="Colombia" value="key4" />
                    </Picker>
                </Item>
                <Item floatingLabel style={{marginBottom:8}}>
                    <Label>Departamento</Label>
                    <Input />
                </Item>
                <Item floatingLabel style={{marginBottom:8}}>
                    <Label>Contraseña</Label>
                    <Input />
                </Item>
                <Item floatingLabel last style={{marginBottom:8}}>
                    <Label>Repetir Contraseña</Label>
                    <Input />
                </Item>
                <Button block success style={{margin:4}} onPress={ () => {this.startDrawer} }>
                    <Text>Registrarse</Text>
                </Button >
            </Form>
        </Content>
      </Container>
    );
  }
}