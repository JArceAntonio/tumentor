import React , { Component, Dimensions } from 'react';
import {Image} from 'react-native';
import { Container, Content, Card, CardItem, Text,Left, List, Button, Icon, Body, Header} from 'native-base';
import data from '../exampleData/publicity';
export default class App extends Component {


    constructor(props) {
        super(props);
        // if you want to listen on navigator events, set this up

        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    }

    onNavigatorEvent(event) { // this is the onPress handler for the two buttons together
        if (event.type == 'NavBarButtonPress') { // this is the event type for button presses
            if (event.id == 'menu2') { // this is the same id field from the static navigatorButtons definition
                this.props.navigator.toggleDrawer({
                    side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
                    animated: true, // does the toggle have transition animation or does it happen immediately (optional)
                    to: 'open' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
                });
            }
        }else if (event.type == 'DeepLink'){
            this.props.navigator.resetTo({
                screen: event.link,
                title: 'Tu Mentor'
            })
        }
    }


  render() {
      let navigatorButtons ={
          leftButtons: [
              {
                  title: 'menu',
                  id: 'menu',
                  showAsAction: 'ifRoom',
              }
          ],
          rightButtons: [ {
              title: 'menu',
              id: 'menu2',
              showAsAction: 'ifRoom',
          }],
          animated: true
      };
      this.props.navigator.setButtons(navigatorButtons);
    return (
      <Container >
        <Content padder>
            <List dataArray={data} renderRow={(item) => {
                return (
                    <Card>
                        <CardItem>
                            <Body>
                                <Text style={{fontSize: 20}} >
                                    {item.title}
                                </Text>
                            </Body>
                        </CardItem>
                        <CardItem>
                            <Body>
                            <Image source={item.image}/>
                            <Text>
                                {item.description}
                            </Text>
                            </Body>
                        </CardItem>
                        <CardItem >
                            <Left>
                                <Button icon small info>
                                    <Icon name="menu"/>
                                    <Text>Me Interesa</Text>
                                </Button>
                                <Button icon small style={{marginLeft:5}}>
                                    <Icon name="star"/>
                                    <Text>Asistire</Text>
                                </Button>
                            </Left>
                        </CardItem>
                    </Card>
                );
            }}>
            </List>
        </Content>
      </Container>
    );
  }
}