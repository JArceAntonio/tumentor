import React , { Component } from 'react';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import { Container, Label, Input, Content, Card, CardItem, Text,Left, Body, Right, Icon, Textarea,Button, Form, Item} from 'native-base';

export default class App extends Component {


    constructor(props) {
        super(props);
        // if you want to listen on navigator events, set this up

        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));

    }

    onNavigatorEvent(event) { // this is the onPress handler for the two buttons together
        if (event.type == 'DeepLink'){
            this.props.navigator.resetTo({
                screen: event.link,
                title: 'Tu Mentor'
            })
        }
    }


  render() {
    return (
      <Container >
        <Content padder>
            <Text style={{fontSize: 35, textAlign: 'center'}}>Requerimiento</Text>
            <Card style={{padding: 16}}>
                        <Form>
                            <Item floatingLabel >
                                <Label>Soy</Label>
                                <Input />
                            </Item>
                            <Item floatingLabel >
                                <Label>Busco Asesoria 1</Label>
                                <Input />
                            </Item>
                            <Item floatingLabel >
                                <Label>Tematica</Label>
                                <Input />
                            </Item>
                            <Textarea style={{margin:15, marginTop: 15 }} rowSpan={5} bordered placeholder="Descripcion Previa" />
                            <MapView
                                style={{flex: 1, height: 250, marginBottom: 16}}
                                provider={PROVIDER_GOOGLE}
                                initialRegion={{
                                    latitude: 37.78825,
                                    longitude: -122.4324,
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,
                                }}


                            />
                            <Button block onPress={ () => {
                                this.props.navigator.push({
                                    screen: "AsesoresScreen",
                                    title: "Tu Mentor"
                                });
                            }}>
                                <Text>
                                    Buscar
                                </Text>
                            </Button>
                        </Form>


            </Card>
        </Content>
      </Container>
    );
  }
}